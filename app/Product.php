<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public $timestamps = false;
    protected $fillable = ['name', 'description', 'path_image', 'user_name', 'user_email', 'user_phone', 'cities_id'];
}