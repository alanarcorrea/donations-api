<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
        	'name' => 'Lucas',
			'email' => 'lucaslimapel@gmail.com',
        	'phone' => '981207814',
        	'cpf' => '03091952096',
        	'cep' => '96030680',
        	'address' => 'José Lins do Rego',
			'neighborhood' => 'Fragata',
        	'city_id' => 1
        ]);
    }
}
