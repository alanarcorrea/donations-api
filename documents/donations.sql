-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema donations
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema donations
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `donations` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `donations` ;

-- -----------------------------------------------------
-- Table `donations`.`cities`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `donations`.`cities` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `donations`.`products`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `donations`.`products` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(150) NOT NULL,
  `description` VARCHAR(350) NOT NULL,
  `path_image` VARCHAR(1000) NOT NULL,
  `user_name` VARCHAR(150) NOT NULL,
  `user_email` VARCHAR(150) NOT NULL,
  `user_phone` VARCHAR(25) NOT NULL,
  `cities_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_products_cities1_idx` (`cities_id` ASC),
  CONSTRAINT `fk_products_cities1`
    FOREIGN KEY (`cities_id`)
    REFERENCES `donations`.`cities` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `donations`.`proposals`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `donations`.`proposals` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `description` VARCHAR(350) NOT NULL,
  `product_id` INT NOT NULL,
  `user_name` VARCHAR(150) NOT NULL,
  `user_email` VARCHAR(150) NOT NULL,
  `user_phone` VARCHAR(25) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_proposal_product1_idx` (`product_id` ASC),
  CONSTRAINT `fk_proposal_product1`
    FOREIGN KEY (`product_id`)
    REFERENCES `donations`.`products` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
