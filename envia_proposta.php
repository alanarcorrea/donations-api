<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "donations";

$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
} 

$nome = htmlspecialchars($_POST["nome"]);
$email = htmlspecialchars($_POST["email"]);
$descricao = htmlspecialchars($_POST["descricao"]);
$produto_id = htmlspecialchars($_POST["produto_id"]);

$sql = "INSERT INTO votos (nome, email, data, candidata_id)
        VALUES ('$nome', '$email', now(), $candidata_id)";


if ($conn->query($sql) === TRUE) {
  $last_id = $conn->insert_id;
  $voto = array("id" => $last_id,
                "nome" => $nome,
                "email" => $email,
                "candidata_id" => $candidata_id);
} else {
  $voto = array("id" => 0,
                "nome" => "",
                "email" => "",
                "candidata_id" => 0);
}

echo json_encode($voto);

$conn->close();