<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "donations";

$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT * FROM products ORDER BY name";
$result = $conn->query($sql);

$produtos = array();

if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
        $produtos[] = array(
          "id" => $row["id"],
          "name" => utf8_encode($row["name"]),
          "description" => utf8_encode($row["description"])
        );
    }
}

$conn->close();

header('content-type: text/javascript');
echo json_encode($produtos);