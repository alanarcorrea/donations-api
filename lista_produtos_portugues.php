<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "donations";

$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT * FROM produtos ORDER BY nome";
$result = $conn->query($sql);

$produtos = array();

if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
        $produtos[] = array(
          "id" => $row["id"],
          "nome" => utf8_encode($row["nome"]),
          "descricao" => utf8_encode($row["descricao"])
        );
    }
}

$conn->close();

header('content-type: text/javascript');
echo json_encode($produtos);