<?php

Route::get('/', function () {
    return view('welcome');
});

Route::post('products/new', 'ProductsController@store')->name('products.store');
Route::resource('products', 'ProductsController');

Route::post('proposals/new', 'ProposalsController@store')->name('proposals.store');
Route::post('proposals/{id}', 'ProposalsController@show')->name('proposals.show');
Route::resource('proposals', 'ProposalsController');

Route::resource('cities', 'CitiesController');